FROM ubuntu:14.04
FROM node:latest
FROM openjdk:10.0.1-jdk

LABEL maintainer Dzmitry Klokau

WORKDIR /root

ADD . /root


RUN ls

#==========================================
# GRADLE
#==========================================
RUN cd /usr/local && \
    curl -L https://services.gradle.org/distributions/gradle-4.7-bin.zip -o gradle-4.7-bin.zip && \
    unzip gradle-4.7-bin.zip && \
    rm gradle-4.7-bin.zip

# Export some environment variables
ENV GRADLE_HOME=/usr/local/gradle-4.7
ENV PATH=$PATH:$GRADLE_HOME/bin 

#==========================================
# INSTALL OTHER TOOLS
#==========================================
RUN apt-get update && \
    apt-get -qqy --no-install-recommends install \
    ca-certificates \
    tzdata \
    zip \
    unzip \
    npm \
    curl \
    wget \
    libqt5webkit5 \
    libgconf-2-4 \
    xvfb \
  && rm -rf /var/lib/apt/lists/*

RUN npm install -g npm
RUN npm -g config set user root

#====================================
# JAVA_HOME
#====================================
ENV PATH=$PATH:$JAVA_HOME/bin
RUN echo $JAVA_HOME


#==========================================
# Install Android SDK
#==========================================
ARG SDK_VERSION=sdk-tools-linux-3859397
ENV SDK_VERSION=$SDK_VERSION
ARG ANDROID_BUILD_TOOLS_VERSION=26.0.0
ENV ANDROID_BUILD_TOOLS_VERSION=$ANDROID_BUILD_TOOLS_VERSION
ARG ANDROID_PLATFORM_VERSION="android-25"
ENV ANDROID_PLATFORM_VERSION=$ANDROID_PLATFORM_VERSION
ENV ANDROID_HOME=/root

RUN wget -O tools.zip https://dl.google.com/android/repository/${SDK_VERSION}.zip --show-progress && \
    unzip tools.zip && rm tools.zip && \
    chmod -R 755 $ANDROID_HOME && \
    chown -R root:root $ANDROID_HOME

ENV PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin
ENV JAVA_OPTS='-XX:+IgnoreUnrecognizedVMOptions --add-modules java.se.ee'

RUN mkdir -p ~/.android && \
    touch ~/.android/repositories.cfg && \
    echo y | $ANDROID_HOME/tools/bin/sdkmanager "tools" && \
    echo y | $ANDROID_HOME/tools/bin/sdkmanager "build-tools;$ANDROID_BUILD_TOOLS_VERSION" && \
    echo y | $ANDROID_HOME/tools/bin/sdkmanager "platform-tools" && \
    echo y | $ANDROID_HOME/tools/bin/sdkmanager "platforms;$ANDROID_PLATFORM_VERSION" && \
    echo y | $ANDROID_HOME/tools/bin/sdkmanager "emulator" && \
    echo y | $ANDROID_HOME/tools/bin/sdkmanager "system-images;$ANDROID_PLATFORM_VERSION;google_apis;x86"


#    echo y | $ANDROID_HOME/tools/bin/sdkmanager "system-images;$ANDROID_PLATFORM_VERSION;google_apis;armeabi-v7a" && \
#    echo y | $ANDROID_HOME/tools/bin/sdkmanager "system-images;$ANDROID_PLATFORM_VERSION;google_apis;x86_64" && \
#    echo y | $ANDROID_HOME/tools/bin/sdkmanager "extras;google;m2repository" && \
#    echo y | $ANDROID_HOME/tools/bin/sdkmanager "extras;android;m2repository"

#==========================================
# Installing emulator dependencies
#==========================================
RUN dpkg --add-architecture i386 && \
    apt-get -qqy update && \
    apt-get -qqy --no-install-recommends install \
    libc6-i386 \
    lib32stdc++6 \
    lib32gcc1 \
    lib32z1 \ 
    libx11-dev \
    libpulse0 \
    libpulse0:i386 \
    libxdamage-dev \
    qemu-kvm 

RUN apt-get update && echo Y | apt-get install goldendict

ENV PATH=$PATH:$ANDROID_HOME/platform-tools:$ANDROID_HOME/build-tools
ENV SHELL /bin/bash

#==========================================
# CREATE_ANDROID_EMULATOR
#==========================================
RUN echo no | $ANDROID_HOME/tools/bin/avdmanager create avd -n AE7 -k "system-images;android-25;google_apis;x86"


#====================================
# APPIUM
#====================================
ARG APPIUM_VERSION=1.7.2
ENV APPIUM_VERSION=$APPIUM_VERSION

RUN npm install -g appium@${APPIUM_VERSION} 


#====================================
# RESULTS
#====================================
RUN $ANDROID_HOME/tools/bin/avdmanager list avd
RUN appium -v
RUN echo $JAVA_HOME
RUN echo $ANDROID_HOME
